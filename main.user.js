// ==UserScript==
// @name        Wikipedia Custom Skin
// @description Userscript that changes the skin for Wikipedia automatically.
// @namespace   https://codeberg.org/archeite/wikipedia-change-skin
// @downloadURL https://codeberg.org/archeite/wikipedia-change-skin/raw/branch/main/userscript.js
// @icon        https://wikipedia.org/static/favicon/wikipedia.ico
// @match       *://*.wikipedia.org/*
// @run-at      document-start
// @grant       none
// @version     1.0.0
// @author      archeite
// ==/UserScript==

// Skins:
//     Vector 2022:    vector2022 (DEFAULT)
//     Vector Legacy:  vector
//     MonoBook:       monobook
//     Timeless:       timeless
//     Minerva Neue:   minerva
const skin = "vector2022"

if (!(/[?&]useskin=/).test(location.search)) {
  location.search += (location.search ? "&" : "?") + `useskin=${skin}`;
}
